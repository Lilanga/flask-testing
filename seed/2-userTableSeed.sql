INSERT INTO Users (id, name, description)
VALUES('whitewolf','Geralt of Rivia','Traveling monster slayer for hire'),
('m31a3n6sion','Lara Croft','Highly intelligent and athletic English archaeologist'),
('smb3igiul','Mario','Italian plumber who really likes mushrooms'),
('nohalflife3','Gordon Freeman','Physicist with great shooting skills');