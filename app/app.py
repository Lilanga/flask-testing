from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
import json

application = Flask(__name__)
db = SQLAlchemy(application)
# application.config['BUNDLE_ERRORS'] = True
application.config.from_pyfile('config.py')
api = Api(application)
CORS(application, send_wildcard=True)

with open("./users.json", "r") as f:
    users = json.load(f)

class arithmetic(Resource):
    def post(self):
        try:
            self.parser = reqparse.RequestParser()
            self.parser.add_argument('x', type=int, required=True, location='args', help='X Value')
            self.parser.add_argument('y', type=int, required=True, location='args', help='Y Value')
            self.parser.add_argument('operation', type=str, location='args', help='Mathematical Operators')
            self.args = self.parser.parse_args()

            x = (self.args['x'])
            y = (self.args['y'])
            ops = (self.args['operation'])

            if ops == '-':
                return {'results': x - y}
            elif ops == '*':
                return {'results': x * y}
            elif ops == '/':
                return {'results': x / y}
            else:
                return {'results': x + y}
            
        except Exception as e:
            return {'status': str(e)}

class Users(Resource):
    def get(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('id', type=str, required=False, location='args', help='id of the user')
        self.args = self.parser.parse_args()
        
        try:
            if self.args['id'] is None:
                users = User.query.all()
                return  jsonify([u.json() for u in users])
            user = User.query.filter_by(id = self.args['id']).first()
            if user is None:
                return 'User not found', 404
            return  jsonify(user.json())
        except Exception as e:
            return(str(e)), 500

    def post(self):
        try:
            data = request.get_json(force=True)
            
            user = User(data['id'],data['name'],data['description'])
            db.session.add(user)
            db.session.commit()
            return ('created'), 201
        except Exception as e:
            return(str(e)), 500

    def put(self):
        try:
            self.parser = reqparse.RequestParser()
            self.parser.add_argument('id', type=str, required=True, location='args', help='id of the user')
            self.args = self.parser.parse_args()
            
            data = request.get_json(force=True)
            
            user = User.query.filter_by(id = self.args['id']).first()
            
            if not 'name' in data and not 'description' in data:
                return('nothing to update')
            
            if 'name' in data:
                user.name = data['name']
                
            if 'description' in data:
                user.description = data['description']

            db.session.commit()
            return ('updated')
        except Exception as e:
            return(str(e)), 500
    
    def delete(self):
        try:
            self.parser = reqparse.RequestParser()
            self.parser.add_argument('id', type=str, required=True, location='args', help='id of the user')
            self.args = self.parser.parse_args()
            
            user = User.query.filter_by(id = self.args['id']).first()
            db.session.delete(user)
            db.session.commit()
            return ('deleted')
        except Exception as e:
            return(str(e)), 500

class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.String(), primary_key=True)
    name = db.Column(db.String())
    description = db.Column(db.String())

    def __init__(self, id, name, description):
        self.id = id
        self.name = name
        self.description = description

    def __repr__(self):
        return '<id {}>'.format(self.id)
    
    def json(self):
        return {
            'id': self.id, 
            'name': self.name,
            'description': self.description
        }        
        
api.add_resource(arithmetic, '/arithmetic')
api.add_resource(Users, '/users')

if __name__ == '__main__':
    application.run(host='0.0.0.0', threaded=True)