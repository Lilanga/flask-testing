import os

basedir = os.path.abspath(os.path.dirname(__file__))

DEBUG = False
SECRET_KEY='resyncdev'
CSRF_ENABLED= True

SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']