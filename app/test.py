from app.app import application
import unittest

class TestArithmatic(unittest.TestCase):
    def setUp(self):
        self.app = application.test_client()
        
    def test_add_numbers(self):
        """
        Test the result of adding two numbers
        """
        data = {
            'x': '1',
            'y': '2',
            'operation': '+'}
        
        response = self.app.post('/arithmetic', query_string = data)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.json['results'], 3)
        
    def test_deduct_numbers(self):
        """
        Test the result of deduction of two numbers
        """
        data = {
            'x': '2',
            'y': '2',
            'operation': '-'}
        
        response = self.app.post('/arithmetic', query_string = data)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.json['results'], 0)
    
    def test_multiply_numbers(self):
        """
        Test the result of the adding operation of two numbers
        """
        data = {
            'x': '2',
            'y': '3',
            'operation': '*'}
        
        response = self.app.post('/arithmetic', query_string = data)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.json['results'], 6)
    
    def test_division_numbers(self):
        """
        Test the result of the division of two numbers
        """
        data = {
            'x': '4',
            'y': '2',
            'operation': '/'}
        
        response = self.app.post('/arithmetic', query_string = data)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.json['results'], 2)

    def test_defalut_add_numbers(self):
        """
        Test the result of the default operation of two numbers
        """
        data = {
            'x': '4',
            'y': '3'}
        
        response = self.app.post('/arithmetic', query_string = data)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.json['results'], 7)
        
if __name__ == '__main__':
    unittest.main()